package com.esalinas.ecompass;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private static final float TRESHOLD = 1.5f;
    private static final float RTD = -57.295779513f;

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mMagnetictField;

    private float[] mAccelerometerValues = new float[3];
    private float[] mMagnetictFieldValues = new float[3];

    private float[] mMatrixR = new float[9];
    private float[] mMatrixI = new float[9];
    private float[] mMatrixValues = new float[3];

    private float mE = 0f;

    private ImageView mTool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        getSupportActionBar().hide();

        setContentView(R.layout.activity_main);

        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMagnetictField = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        mTool = (ImageView) findViewById(R.id.imgvTool);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startSensor();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopSensor();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        switch(event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                for (int i = 0; i < 3; i++) mAccelerometerValues[i] = event.values[i];

                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                for (int i = 0; i < 3; i++) mMagnetictFieldValues[i] = event.values[i];
                break;
            default: return;
        }

        boolean success = SensorManager.getRotationMatrix(
                mMatrixR,
                mMatrixI,
                mAccelerometerValues,
                mMagnetictFieldValues);

        if(!success) return;

        SensorManager.getOrientation(mMatrixR, mMatrixValues);

        final float e = mMatrixValues[0] * RTD;

        if(Math.abs(Math.abs(mE) - Math.abs(e)) <= TRESHOLD) return;

        RotateAnimation ra = new RotateAnimation(mE, e,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

        ra.setDuration(250);

        ra.setFillAfter(true);

        mTool.startAnimation(ra);
        mE = e;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private void startSensor() {
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mMagnetictField, SensorManager.SENSOR_DELAY_GAME);
    }

    private void stopSensor() {
        mSensorManager.unregisterListener(this);
        mSensorManager.unregisterListener(this);
    }
}
